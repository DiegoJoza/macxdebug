# Xdebug php 5.6 para MacOs Movaje utilizando XAMPP

Requerimientos
Herramientas de línea de comandos para Xcode 
https://developer.apple.com/downloads
Tutorial de instalación de Ruby
Homebrew

Use homebrew para instalar la utilidad autoconf para producir scripts de configuración.
# install autoconf
```sh
brew install autoconf
```
# phpize
XAMPP viene con phpize que queremos utilizar para preparar el entorno de compilación para la extensión PHP de Xdebug. Si se instala otra versión de phpize, será necesario cambiar su nombre. Compruebe si phpize está instalado en / usr / bin:
```sh
cd /usr/bin
ls -al | grep phpize
-rwxr-xr-x     1 root   wheel      4508 Sep  9  2014 phpize
```

A partir de OS X 10.11 El Capitán, los archivos y procesos del sistema están protegidos con la nueva función de Protección de integridad del sistema. Por lo tanto, las siguientes escrituras /usr/binno están permitidas a menos que SIP esté deshabilitado ( no recomendado ). Para una solución, intente agregar esta variable de entorno a .bash_profile:export PATH=/Applications/XAMPP/xamppfiles/bin:$PATH
Si la búsqueda de grep devuelve phpize, similar a lo que se muestra arriba, entonces renómbrelo a phpize_bak.

# rename / backup phpize
```sh
sudo mv phpize phpize_bak
```
Cree un nuevo enlace simbólico en / usr / bin para apuntar a la versión de phpize de XAMPP.

# navigate to the /usr/bin directory
```sh
cd /usr/bin
```
# create symbolic link to XAMPP phpize
```sh
sudo ln -s /Applications/XAMPP/bin/phpize-5.6.11 phpize
```
# check phpize version
```sh 
cd /
phpize -v
Configuring for:
PHP Api Version:         20131106
Zend Module Api No:      20131226
Zend Extension Api No:   220131226
```
# Instalación de Xdebug
Abra el archivo phpinfo.php de XAMPP en un navegador web, por ejemplo, http: //localhost/dashboard/phpinfo.php . En otra ventana o pestaña del navegador, abra https://xdebug.org/wizard.php y copie el contenido de la página phpinfo en la primera ventana o pestaña y péguelo en el cuadro de texto en la página xdebug.org. Luego envíe para su análisis para determinar la fuente de Xdebug para descargar. La respuesta debe ser un resumen e instrucciones de instalación adaptadas.

# Descargar xdebug-2.4.0.tgz 
continuación ...

# Desembale el archivo descargado
``` sh
# navigate to the downloaded file
cd ~/Downloads
tar -xvzf xdebug-2.4.0.tgz
cd xdebug-2.4.0
```

# correr phpize
```shg
phpize
```
# example output
Configuring for:
PHP Api Version:         20131106
Zend Module Api No:      20131226
Zend Extension Api No:   220131226

# Configurar con la configuración php de XAMPP.
```sh
./configure --enable-xdebug --with-php-config=/Applications/XAMPP/xamppfiles/bin/php-config
```

# correr 
```sh
make
```

En las Instrucciones de instalación adaptadas de xdebug.org , localice el paso después de make que contiene el comando cp y las rutas para copiar el archivo de extensión xdebug.so en XAMPP, por ejemplo,

```sh
sudo cp modules/xdebug.so /Applications/XAMPP/xamppfiles/lib/php/extensions/no-debug-non-zts-20131226
```

Edite / Aplicaciones / XAMPP / xamppfiles / etc / php.ini
Después de "; zend_extension" 
Agregue la configuración zend_extension de las instrucciones de xdebug.org, por ejemplo:

```sh 
zend_extension=/Applications/XAMPP/xamppfiles/lib/php/extensions/no-debug-non-zts-20131226/xdebug.so
```

Además, agregue estas opciones de configuración de xdebug en php.ini después de la configuración de zend_extension:

```sh 
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_host=localhost
xdebug.remote_autostart = 1
xdebug.remote_port=9000
xdebug.show_local_vars=1
xdebug.remote_log=/Applications/XAMPP/logs/xdebug.log

;---------------------------------
; uncomment these dev environment
; specific settings as needed
;---------------------------------
;xdebug.idekey = "netbeans-xdebug"
;xdebug.idekey = "sublime.xdebug"

;some pages in your Drupal site will not work default = 100
;xdebug.max_nesting_level=256
```

Optional - for development only
Find max_execution_time and set to unlimited:
```sh
max_execution_time=0
```

Restart Apache using XAMPP’s manager-osx

Reload http://localhost/dashboard/phpinfo.php and verify that xdebug section exists. Another verification method is to copy and paste the phpinfo into the form at https://xdebug.org/wizard.php and resubmit it for analysis.


